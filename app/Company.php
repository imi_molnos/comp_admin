<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['id', 'name', 'email','address', 'logo', 'website'];

    /*public function users(){
        return $this->hasMany('App\User');
    }*/
}
