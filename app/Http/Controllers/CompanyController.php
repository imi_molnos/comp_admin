<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    public function index()
    {
        /*Pagination*/
        $c = Company::paginate(3);
        return view('companies', compact ('c'));
    }

    /*------ EDIT ------*/
    public function edit($id)
    {
        $comp = Company::find($id);
        return view('edit', compact('comp', 'id'));
    }

    public function update(Request $request, $id)
    {
        $comp = Company::find($id);
        $comp->name = $request->get('name');
        $comp->address = $request->get('address');
        $comp->email = $request->get('email');
        $comp->logo = $request->get('logo');
        $comp->website = $request->get('website');
        $comp->save();

        return redirect()->route('companies.index')->with('success', 'Company updated successfully');
    }

    /*------ DELETE ------*/
    public function destroy($id)
    {
        $comp = Company::find($id);
        $comp->delete();

        return redirect()->route('companies.index')->with('success', 'Company deleted successfully');
    }

    /* ------ CREATE ------*/
    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'logo' => 'required',
            'website' => 'required',
        ]);

        /*$comp = $request->all();
        if($request -> hasfile('logo')){
            $request->logo->storeAs('companies', $comp['logo']);
            $pathToFile = storage_path('storage/app/public' . $comp->logo);
        }*/

        Company::create($request->all());

        return redirect()->route('companies.index')
            ->with('success', 'Company created successfully.');
    }


}
