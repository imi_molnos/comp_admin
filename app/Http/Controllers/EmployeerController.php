<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employees;

class EmployeerController extends Controller
{
    public function index()
    {
        $emp = Employees::all();
        return view('employees', ['e' => $emp]);
    }
}
