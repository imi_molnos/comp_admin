<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'name' => 'Laravel Daily',
            'address' => 'New York',
            'email' => 'www@email.com',
            'logo' => 'storage/app/public/google.jpg',
            'website' => 'www.laraveldayli.com',
        ]);

        DB::table('companies')->insert([
            'name' => 'Facebook',
            'address' => 'Los Angeles',
            'email' => 'www@email.com',
            'logo' => 'storage/app/public/google.jpg',
            'website' => 'www.laraveldayli.com',
        ]);

        DB::table('companies')->insert([
            'name' => 'Google Inc',
            'address' => 'Rio',
            'email' => 'www@email.com',
            'logo' => 'storage/app/public/google.jpg',
            'website' => 'www.googleinc.com',
        ]);

        DB::table('companies')->insert([
            'name' => 'Spotify',
            'address' => 'Old York',
            'email' => 'www@email.com',
            'logo' => 'storage/app/public/google.jpg',
            'website' => 'www.spot.com',
        ]);
    }
}

