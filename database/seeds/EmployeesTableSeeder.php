<?php

use App\Company;

use App\User;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'firstname' => 'John',
            'lastname' => 'Doe',
            'company_id' => 1,
            'email' => 'johndoe@email.com',
            'phone' => 123321,
        ]);
    }
}

