@extends('layouts.app')
@section('content')
<div class="container">
<div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Companies</div>
                <br>
                <div class="card-body">
                <div class="container">
                    <form action="{{route('companies.create')}}" method="get">
                    @csrf
                     <button class="btn btn-success" type="submit"><i class="fa fa-plus fa-lg" aria-hidden="true"></i>Create new company</button>
                     <a href="{{route('companies.employees')}}" class="btn btn-primary" type="button"><i class="fa fa-plus fa-lg" aria-hidden="true"></i>Employees</a>
                    </form>
                    <br>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">Companies list</div>
                            <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Website</th>
                                    <th scope="col">Email</th>
                                    <th scope="col"></th>
                                    </tr>
                                </thead>
                             <tbody>
                                @foreach($c as $comp)
                                <tr>
                                <td>{{$comp->name}}</td>
                                <td>{{$comp->address}}</td>
                                <td>{{$comp->website}}</td>
                                <td>{{$comp->email}}</td>
                                <td><a href="{{action('CompanyController@edit',$comp->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit fa-lg" aria-hidden="true"></i>Edit</a>
                                    <form action="{{action('CompanyController@destroy', $comp->id)}}" method="post" onsubmit="return confirm('Are you sure?');">
                                    @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button class="btn btn-danger btn-sm" type="submit"> <i class="fa fa-remove fa-lg" aria-hidden="true"></i>Delete</button>
                                    </form>
                                </td>
                                </tr>
                                 @endforeach
                                 </div>
                            </tbody>
                            </table>
                        </div>
                 </div>
                 {{$c->links()}}
            </div>
        </div>
      </div>
     </div>
    </div>
  </div>
@endsection

