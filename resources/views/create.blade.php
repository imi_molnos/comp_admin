@extends('layouts.app')
@section('content')
        <div class="col-md-6">
                <form action="{{action('CompanyController@store')}}" method="post">
                    @csrf
                    <div class="row form-group">
                        <div class="col-md-10">
                            <label for="">Name: </label>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-10">
                            <label for="">Address: </label>
                            <input type="text" name="address" class="form-control" required>
                        </div>
                    </div>
                      <div class="row form-group">
                        <div class="col-md-10">
                            <label for="">Email: </label>
                            <input type="email" name="email" class="form-control" required>
                        </div>
                    </div>
                      <div class="row form-group">
                        <div class="col-md-10">
                            <label for="">Logo: </label>
                            <input type="file" name="logo" class="form-control" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-10">
                            <label for="">Website: </label>
                            <input type="text" name="website" class="form-control" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-10">
                        <button type="submit" class="btn btn-success">Create a new company</button>
                        </div>
                    </div>
                </form>
            </div>
         @endsection
