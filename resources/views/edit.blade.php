@extends('layouts.app')
@section('content')
<div class="col-md-6">
<div class="panel panel-success">
    <div class="panel-heading">
        <i aria-hidden="true" class="fa fa-th-list fa-lg">
        </i>
    </div>
    <div class="panel-body">
        <form action="{{route('companies.update',$comp->id)}}" method="post">
            @csrf
            <div class="form-group">
                <label for="title">
                    Name:
                </label>
                <input class="form-control" name="name" type="text" value="{{$comp->name}}"/>
            </div>
            <div class="form-group">
                <label for="title">
                    Address:
                </label>
                <input class="form-control" name="address" type="text" value="{{$comp->address}}"/>
            </div>
            <div class="form-group">
                <label for="title">
                    E-mail:
                </label>
                <input class="form-control" name="email" type="email" value="{{$comp->email}}"/>
            </div>
            <div class="form-group">
                <label for="title">
                    Logo:
                </label>
                <input class="form-control" name="logo" type="text" value="{{$comp->logo}}"/>
            </div>
            <div class="form-group">
                <label for="title">
                    Website:
                </label>
                <input class="form-control" name="website" type="text" value="{{$comp->website}}"/>
            </div>
            <button class="btn btn-primary" type="submit">
                Update
            </button>
        </form>
    </div>
</div>
</div>
@endsection


