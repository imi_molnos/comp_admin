<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
         <!--   <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input class="form-control" placeholder="Search..." type="text">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <i class="fa fa-search">
                                </i>
                            </button>
                        </span>
                    </input>
                </div>

            </li> -->
            <li>
                <a href="{{route('dashboard')}}">
                    <i class="fa fa-dashboard fa-fw">
                    </i>
                    Dashboard
                </a>
            </li>

            <li>
                <a href="{{route('companies.index')}}">
                    <i class="fa fa-user fa-fw">
                    </i>
                    Companies
                </a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
