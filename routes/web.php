<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/companies', 'CompanyController@index')->name('companies.index');
Route::get('/edit/company/{id}', 'CompanyController@edit')->name('companies.edit');
Route::post('/edit/company/{id}', 'CompanyController@update')->name('companies.update');
Route::delete('/delete/company/{id}', 'CompanyController@destroy')->name('companies.destroy');
Route::get('/create/company', 'CompanyController@create')->name('companies.create');
Route::post('/create/company', 'CompanyController@store')->name('companies.store');
Route::get('/employees', 'EmployeerController@index')->name('companies.employees');


Auth::routes(['register' => false]);
